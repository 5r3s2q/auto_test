from pages import SearchHelper

def test_search(browser):
    main_page = SearchHelper(browser)
    main_page.go_to_site()
    main_page.enter_word("МТС Банк")
    main_page.click_on_the_search_button()
    element = main_page.check_navigation_bar()
    main_page.go_to_url(element)
    tab = main_page.check_tabs()

    main_page.go_to_url(tab)
    form = main_page.check_contribution()
    main_page.go_to_url(form)
    main_page.fill_form()
    main_page.go_to_url(element)