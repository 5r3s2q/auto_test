from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

from base_app import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

class Locators:
    LOCATOR_SEARCH_FIELD = (By.CLASS_NAME, "gLFyf")
    LOCATOR_NAVIGATION_BAR = (By.CSS_SELECTOR, "a")
    LOCATOR_TAB = (By.LINK_TEXT, "Вклады")
    LOCATOR_FORM = (By.XPATH, '//div[contains(@class, "depo-note-links")]//a[contains(@href, "__form")]')

    LOCATOR_FORM_FIO_FIELD = (By.ID, 'dadataFIO')
    LOCATOR_FORM_PHONE_FIELD = (By.ID, 'mts2-section-form__mobilephone')
    LOCATOR_FORM_CITY_FIELD = (By.ID, 'cities')
    LOCATOR_FORM_DEPARTMENT_FIELD = (By.ID, 'affiliates')
    LOCATOR_FORM_EMAIL_FIELD = (By.ID, 'mts2-section-form__email')
    LOCATOR_FORM_PROMO_FIELD = (By.ID, 'mts2-section-form__promo')

class SearchHelper(BasePage):

    def enter_word(self, word):
        search_field = self.find_element(Locators.LOCATOR_SEARCH_FIELD)
        search_field.click()
        search_field.send_keys(word)
        return search_field

    def click_on_the_search_button(self):
        return self.find_element(Locators.LOCATOR_SEARCH_FIELD,time=2).send_keys(Keys.ENTER)

    def check_navigation_bar(self):
        all_list = self.find_elements(Locators.LOCATOR_NAVIGATION_BAR,time=2)

        for x in all_list:
            if x.get_attribute("href") == 'https://www.mtsbank.ru/':
                return x.get_attribute("href")

    def check_tabs(self):
        element = self.find_element(Locators.LOCATOR_TAB,time=2)

        return element.get_attribute("href")


    def check_contribution(self):
        x = self.find_element(Locators.LOCATOR_FORM, time=2)
        return x.get_attribute("href")

    def fill_form(self, fio="Тест Тест Тест", phone="8800853535", city="Москва",
                  email='test@gmail.com', department='Отделение', promo=85858158):

        fio_field = self.find_element(Locators.LOCATOR_FORM_FIO_FIELD)
        fio_field.click()
        fio_field.send_keys(fio)

        phone_field = self.find_element(Locators.LOCATOR_FORM_PHONE_FIELD)
        phone_field.click()
        phone_field.send_keys(phone)

        city_field = self.find_element(Locators.LOCATOR_FORM_CITY_FIELD)
        city_field.click()
        city_field.send_keys(city)

        try:
            depart_field = Select(self.find_element(Locators.LOCATOR_FORM_DEPARTMENT_FIELD))
            depart_field.select_by_value(department)
        except NoSuchElementException:
            pass

        email_field = self.find_element(Locators.LOCATOR_FORM_EMAIL_FIELD)
        email_field.click()
        email_field.send_keys(email)

        promo_field = self.find_element(Locators.LOCATOR_FORM_PROMO_FIELD)
        promo_field.click()
        promo_field.send_keys(promo)